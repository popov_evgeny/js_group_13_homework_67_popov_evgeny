import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FoodService } from '../shared/food.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MealModel } from '../shared/meal.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-edit-ingestion',
  templateUrl: './add-edit-ingestion.component.html',
  styleUrls: ['./add-edit-ingestion.component.css']
})

export class AddEditIngestionComponent implements OnInit, OnDestroy {
  @ViewChild('form') ingestionForm!: NgForm;
  postIngestionSubscription!: Subscription;
  loading!: boolean;
  isEdit = false;
  editedId = '';

  constructor(
    private foodService: FoodService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.postIngestionSubscription = this.foodService.addEditIngestion.subscribe( (isLoading:boolean) => {
      this.loading = isLoading;
    });
    this.route.data.subscribe(data => {
      const ingestion = <MealModel | null>data.ingestion;

      if (ingestion) {
        this.isEdit = true;
        this.editedId = ingestion.id;
        this.setFormValue({
          date: ingestion.date,
          select: ingestion.time,
          description: ingestion.description,
          calorie: ingestion.calorie,
        });
      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          date: this.foodService.getDate(),
          select: '',
          description: '',
          calorie: '',
        });
      }
    })
  }

  setFormValue(value: { [key: string]: any }) {
    setTimeout(() => {
      this.ingestionForm.form.setValue(value);
    })
  }

  saveIngestion() {
    const id = this.editedId || Math.random().toString();
    const ingestion = new MealModel(id, this.ingestionForm.value.date, this.ingestionForm.value.select, this.ingestionForm.value.description, this.ingestionForm.value.calorie);
    const next = () => {
      this.foodService.fetchAllIngestion();
      void this.router.navigate(['/']);
    }
    if (this.isEdit) {
      this.foodService.editIngestion(ingestion).subscribe(next);
    } else {
      this.foodService.addIngestion(ingestion).subscribe(next);
    }
  }

  ngOnDestroy() {
    this.postIngestionSubscription.unsubscribe();
  }
}
