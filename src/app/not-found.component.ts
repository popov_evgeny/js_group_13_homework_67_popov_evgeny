import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `<h1>Not found!</h1>`,
  styles: [`
    h1 {
      color: black;
      font-size: 80px;
      font-style: italic;
      padding: 50px;
    }
  `]
})
export class NotFoundComponent {}
