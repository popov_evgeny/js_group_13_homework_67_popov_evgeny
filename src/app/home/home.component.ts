import { Component, OnDestroy, OnInit } from '@angular/core';
import { MealModel } from '../shared/meal.model';
import { Subscription } from 'rxjs';
import { FoodService } from '../shared/food.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  arrayIngestion: MealModel[] | null = null;
  changeArrayIngestionSubscription!: Subscription;
  fetchingArrayIngestionSubscription!: Subscription;
  totalCalorie!: any;
  loading!: boolean;
  ingestionId = '';
  arrayIsEmpty!: boolean;

  constructor(private foodService: FoodService) {}

  ngOnInit(): void {
    this.foodService.getArrIngestion();
    this.changeArrayIngestionSubscription = this.foodService.changeArrayIngestion.subscribe( (arrayIngestion:MealModel[]) => {
      this.arrayIngestion = arrayIngestion;
      this.arrayIsEmpty = this.arrayIngestion.length === 0;
      this.getTotalCalorie();
    });
    this.fetchingArrayIngestionSubscription = this.foodService.fetchingArrayIngestion.subscribe( (isLoading:boolean) => {
      this.loading = isLoading;
    });
    this.foodService.fetchAllIngestion();
  }

  getTotalCalorie() {
    let date = this.foodService.getDate();
    if (this.arrayIngestion) {
      this.totalCalorie = this.arrayIngestion.reduce((acc: any, ingestion: MealModel) => {
        if (date === ingestion.date) {
          acc += ingestion.calorie;
        }
        return acc;
      }, 0);
    }
  }

  onRemoveIngestion(id: string) {
    this.ingestionId = id;
    this.foodService.onRemoveIngestion(id);
  }

  ngOnDestroy() {
    this.changeArrayIngestionSubscription.unsubscribe();
    this.fetchingArrayIngestionSubscription.unsubscribe();
  }
}
