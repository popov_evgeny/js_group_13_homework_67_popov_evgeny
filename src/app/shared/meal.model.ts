export class MealModel {
  constructor(
    public id: string,
    public date: string,
    public time: string,
    public description: string,
    public calorie: number
  ) {}
}
