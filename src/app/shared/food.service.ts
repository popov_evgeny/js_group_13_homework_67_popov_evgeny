import { Injectable } from '@angular/core';
import { MealModel } from './meal.model';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';


@Injectable()

export class FoodService {
  private arrayIngestion: MealModel[] = [];
  changeArrayIngestion = new Subject<MealModel[]>();
  fetchingArrayIngestion = new Subject<boolean>();
  addEditIngestion = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  getDate() {
    return `${new Date().getFullYear()}-${new Date().getMonth()+1}-${new Date().getDate()}`;
  }

  getArrIngestion() {
    return this.arrayIngestion.slice();
  }

  getSortArrayIngestion(): any {
    if (this.arrayIngestion) {
      return this.arrayIngestion.sort((a:MealModel, b:MealModel) => {
        return <any>new Date(b.date) - <any>new Date(a.date);
      });
    }
  }

  fetchAllIngestion() {
    this.fetchingArrayIngestion.next(true);
    this.http.get<{ [id: string]: MealModel }>(`https://project-f65ad-default-rtdb.firebaseio.com/food.json`).pipe(map(result => {
      if (result === null) {
        return [];
      }
      return Object.keys(result).map(id => {
        const meal = result[id];
        return new MealModel(id, meal.date, meal.time, meal.description, meal.calorie);
      })
    })).subscribe(meal => {
      this.arrayIngestion = meal;
      this.getSortArrayIngestion();
      this.changeArrayIngestion.next(this.arrayIngestion.slice());
      this.fetchingArrayIngestion.next(false);
    }, () => {
      this.fetchingArrayIngestion.next(false);
    });
  }

  fetchIngestion(id: string){
    return this.http.get<MealModel | null>(`https://project-f65ad-default-rtdb.firebaseio.com/food/${id}.json`).pipe(map(result => {
      if (!result) return null;
      return new MealModel(
        id, result.date, result.time,
        result.description, result.calorie
      );
    }));
  }

  addIngestion(ingestion: MealModel) {
    this.addEditIngestion.next(true);
    const body = {
      date: ingestion.date,
      time: ingestion.time,
      description: ingestion.description,
      calorie: ingestion.calorie,
    };
    return this.http.post('https://project-f65ad-default-rtdb.firebaseio.com/food.json', body).pipe(tap(() => {
      this.addEditIngestion.next(false);
    }, () => {
      this.addEditIngestion.next(false);
    }));
  }

  editIngestion(ingestion: MealModel) {
    this.addEditIngestion.next(true);
    const body = {
      date: ingestion.date,
      time: ingestion.time,
      description: ingestion.description,
      calorie: ingestion.calorie,
    };
    return this.http.put(`https://project-f65ad-default-rtdb.firebaseio.com/food/${ingestion.id}.json`, body ).pipe(tap(() => {
      this.addEditIngestion.next(false);
    }, () => {
      this.addEditIngestion.next(false);
    }));
  }

  onRemoveIngestion(id: string) {
    this.http.delete(`https://project-f65ad-default-rtdb.firebaseio.com/food/${id}.json`).subscribe(() => {
      this.fetchAllIngestion();
    });
  }
}
