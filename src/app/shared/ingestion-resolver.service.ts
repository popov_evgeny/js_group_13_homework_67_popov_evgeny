import { MealModel } from './meal.model';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { EMPTY, Observable, of } from 'rxjs';
import { FoodService } from './food.service';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IngestionResolverService implements Resolve<MealModel> {

  constructor(
    private foodService: FoodService,
    private router: Router
    ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<MealModel> | Observable<never>{
    const id = <string>route.params['id'];
    return this.foodService.fetchIngestion(id).pipe(mergeMap(ingestion => {
      if (ingestion) {
        return of (ingestion);
      }
      void this.router.navigate(['/']);
      return EMPTY;
    }))
  }
}
