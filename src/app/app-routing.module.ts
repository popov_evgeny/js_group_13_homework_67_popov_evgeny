import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AddEditIngestionComponent } from './add-edit-ingestion/add-edit-ingestion.component';
import { IngestionResolverService } from './shared/ingestion-resolver.service';
import { NotFoundComponent } from './not-found.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'new-ingestion', component: AddEditIngestionComponent},
  { path: ':id/edit', component: AddEditIngestionComponent, resolve: {ingestion: IngestionResolverService}},
  { path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
